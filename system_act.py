import sys
import os
import inspect
import getpass
import logging.config
from pathlib import Path
import string
import user_in
import subprocess

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='dlightning.log',
                    level=logging.DEBUG)

def warnings():
    if os.path.exists('/run/wifi-country-unset'):
        print('Wi-Fi is disabled because the country is not set.')
        print('Use raspi-config to set the country before use.')
    if os.path.exists('/run/sshwarn'):
        print('Default password hasn\'t been changed. Change it and launch again.')
        sys.exit()


def first_time():
    if os.path.exists('/run/dlightning'):
        return False
    else:
        return True

def home_folder():
    home_folder = os.path.expanduser("~")
    return home_folder


def soft_folder():
    home = os.path.expanduser("~")
    soft_folder = home + "/soft"
    return soft_folder


def pre_folder():
    home = os.path.expanduser("~")
    pre_folder = home + "/soft/prereq"
    return pre_folder


def bitcoin_folder():
    home = os.path.expanduser("~")
    bitcoin_folder = home + "/soft/bitcoin"
    return bitcoin_folder


def lnd_folder():
    home = os.path.expanduser("~")
    lnd_folder = home + "/soft/lnd"
    return lnd_folder


def bitcoin_data_folder():
    home = os.path.expanduser("~")
    btcdir = home + "/.bitcoin"
    return btcdir


def lnd_data_folder():
    home = os.path.expanduser("~")
    lnddir = home + "/.lnd"
    return lnddir

def sudo_call(fn):
    code = inspect.getsource(fn)
    script = 'temp.py'
    with open(script, "w+") as f:
        f.write(code)
        f.write("%s" % (fn.__name__) + '()')
    f.close()
    os.system('sudo python3 ' + script)


def prerequisites():
    import os
    import logging.config
    import user_in
    import subprocess
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='dlightning.log',
                        level=logging.DEBUG)
    soft_folder = '/home/' + (os.environ['SUDO_USER']) + '/soft'
    pre_folder = '/home/' + (os.environ['SUDO_USER']) + '/soft/prereq'
    user_in.color_text.info('Updating sources. Wait a minute.')
    logging.info('Updating sources.')
    subprocess.call(['apt-get', 'update'], stdout=open(os.devnull, 'wb'))
    user_in.color_text.info('Sources updated')
    user_in.color_text.info('Upgrading system. Wait a minute.')
    logging.info('Upgrading system')
    subprocess.call(['apt-get', 'upgrade', '-y'], stdout=open(os.devnull, 'wb'))
    user_in.color_text.info('System upgraded')
    logging.info('System upgraded')
    user_in.color_text.info('Installing prerequisites. This may take some time.')
    logging.info('Installing prerequisites')
    subprocess.call(['apt-get', 'install', 'git', 'python3-pip', 'gnupg2', 'dirmngr', '-y'], stdout=open(os.devnull,
                                                                                                         'wb'))
    # Install nodejs v11 (required by RTL)
    p1 = subprocess.Popen(
        ['curl', '-sL', 'https://deb.nodesource.com/setup_11.x'],
        stdout=subprocess.PIPE, stderr=open(os.devnull, 'wb'))
    p2 = subprocess.Popen(['bash', '-'], stdin=p1.stdout, stdout=open(os.devnull, 'wb'),
                          stderr=open(os.devnull, 'wb'))
    p1.stdout.close()
    p2.communicate()[0]
    subprocess.call(['apt-get', 'install', 'nodejs', '-y'],
                    stdout=open(os.devnull, 'wb'))
    user_in.color_text.info('Prerequisites installed.')
    # Create services
    os.chdir(soft_folder)
    subprocess.call(['mv', 'bitcoind.service', '/etc/systemd/system/'], stdout=open(os.devnull, 'wb'))
    subprocess.call(['mv', 'lnd.service', '/etc/systemd/system/'], stdout=open(os.devnull, 'wb'))
    subprocess.call(['systemctl', 'enable', 'bitcoind.service'], stdout=open(os.devnull, 'wb'), stderr=open(os.devnull,
                                                                                                            'wb'))
    subprocess.call(['systemctl', 'enable', 'lnd.service'], stdout=open(os.devnull, 'wb'), stderr=open(os.devnull,
                                                                                                       'wb'))

def TOR_INSTALL():
    import os
    import logging.config
    import user_in
    import subprocess
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='dlightning.log',
                        level=logging.DEBUG)
    user_in.color_text.info('Adding TOR source to sources list')
    logging.info('Adding TOR source to sources list')
    f = open('/etc/apt/sources.list.d/nodesource.list', 'w+')
    f.write('deb https://deb.torproject.org/torproject.org stretch main\n')
    f.write('deb-src https://deb.torproject.org/torproject.org stretch main\n')
    f.close()
    logging.info('TOR source added')
    logging.info('Signing new sources')
    p1 = subprocess.Popen(
        ['curl', 'https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc'],
        stdout=subprocess.PIPE, stderr=open(os.devnull, 'wb'))
    p2 = subprocess.Popen(['gpg', '--import'], stdin=p1.stdout, stdout=open(os.devnull, 'wb'),
                          stderr=open(os.devnull, 'wb'))
    p1.stdout.close()
    p2.communicate()[0]
    p1 = subprocess.Popen(['gpg', '--export', 'A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89'], stdout=subprocess.PIPE,
                          stderr=open(os.devnull, 'wb'))
    p2 = subprocess.Popen(['apt-key', 'add', '-'], stdin=p1.stdout, stdout=open(os.devnull, 'wb'),
                          stderr=open(os.devnull, 'wb'))
    p1.stdout.close()
    p2.communicate()[0]
    user_in.color_text.info('TOR source added')
    logging.info('Sources signed')
    user_in.color_text.info('Updating sources. Wait a minute.')
    logging.info('Updating sources')
    subprocess.call(['apt-get', 'update'], stdout=open(os.devnull, 'wb'))
    user_in.color_text.info('Sources updated')
    logging.info('Sources updated')
    user_in.color_text.info('Installing TOR')
    subprocess.call(['apt-get', 'install', 'tor', '-y'], stdout=open(os.devnull, 'wb'))
    user_in.color_text.info('TOR installed')
    logging.info('TOR installed')
    user_in.color_text.info('Configuring TOR')
    logging.info('Configuring TOR')
    with open('/etc/tor/torrc', 'r') as file:
        filedata = file.read()
        filedata = filedata.replace('#ControlPort 9051', 'ControlPort 9051')
    with open('/etc/tor/torrc', 'w') as file:
        file.write(filedata)
    subprocess.call(['chmod', 'a+r', '/var/run/tor/control.authcookie'], stdout=open(os.devnull, 'wb'))
    subprocess.call(['adduser', 'pi', 'debian-tor'], stdout=open(os.devnull, 'wb'))
    subprocess.call(['service', 'tor', 'restart'], stdout=open(os.devnull, 'wb'))
    user_in.color_text.info('TOR Configured')
    logging.info('TOR configured')


def python_ver():
    if sys.version_info > (3, 0):
        return True
    else:
        return False

def microsd_size():
    total, used, free = shutil.disk_usage("/")
    s = (total // (2 ** 30))
    return s

class create_service():

    def bitcoind():
        f = open(soft_folder() + '/bitcoind.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=Bitcoin daemon\n')
        f.write('After=network.target\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + username() + '\n')
        f.write('\n')
        f.write('Type=forking\n')
        f.write(
            'ExecStart=' + bitcoin_folder() + '/bin/bitcoind -conf=' + bitcoin_data_folder() + '/bitcoin.conf -datadir='
            + bitcoin_data_folder() + ' -pid=/run/bitcoind/bitcoind.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()

    def lnd():
        f = open(soft_folder() + '/lnd.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=LND Lightning Daemon\n')
        f.write('Requires=bitcoind.service\n')
        f.write('After=bitcoind.service\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + username() + '\n')
        # f.write('Group=' + systeminfo.user_group() + '\n')
        f.write('\n')
        f.write('Type=simple\n')
        f.write('ExecStart=' + lnd_folder() + '/lnd\n')
        f.write('PIDFile=' + lnd_data_folder() + '/lnd.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()

    def lnd_tor():
        f = open(soft_folder() + '/lnd.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=LND Lightning Daemon\n')
        f.write('Requires=bitcoind.service\n')
        f.write('After=bitcoind.service\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + username() + '\n')
        # f.write('Group=' + systeminfo.user_group() + '\n')
        f.write('\n')
        f.write('Type=simple\n')
        f.write('ExecStart=' + lnd_folder() + '/lnd --tor.active --tor.v3\n')
        f.write('PIDFile=' + lnd_data_folder() + '/lnd.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()

def create_soft_folder():
    home = os.path.expanduser("~")
    folder = home + '/soft'
    bitcoin_folder = folder + '/bitcoin'
    lnd_folder = folder + '/lnd'
    prereq_folder = folder + '/prereq'
    btcdir = home + "/.bitcoin"
    lnddir = home + "/.lnd"
    bitcoin_path = Path(bitcoin_folder)
    lnd_path = Path(lnd_folder)
    prereq_path = Path(prereq_folder)
    btcdir_path = Path(btcdir)
    lnddir_path = Path(lnddir)
    bitcoin_path.mkdir(parents=True, exist_ok=True)
    lnd_path.mkdir(parents=True, exist_ok=True)
    prereq_path.mkdir(parents=True, exist_ok=True)
    btcdir_path.mkdir(parents=True, exist_ok=True)
    lnddir_path.mkdir(parents=True, exist_ok=True)

def username():
    username = getpass.getuser()
    return username