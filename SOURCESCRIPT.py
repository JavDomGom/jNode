import getpass
import inspect
import json
import math
import os.path
import re
import shutil
import subprocess
import urllib.request
from datetime import datetime
from pathlib import Path
import logging

class systeminfo():

    # will use it in services creation
    def username():
        username = getpass.getuser()
        return username

    # will use it in services creation
    def user_group():
        user_group = subprocess.call(['id', '-gn', systeminfo.username()])
        return user_group

    def home_folder():
        home = os.path.expanduser("~")
        return home


class color_text():  # text formatting

    def info(msg):  # GREEN
        print("\33[1;32;40m", msg, '\033[m')

    def error(msg):  # RED
        print("\033[1;31;40m", msg, '\033[m')

    def warning(msg):  # YELLOW
        print("\033[1;33;40m", msg, '\033[m')

    def change_to_green():
        print("\33[1;32;40m")

    def change_to_blue():
        print("\33[1;32;40m")

    def reset_style():
        print('\033[m')


class github():

    def bitcoin_core_version():
        url = 'https://api.github.com/repos/bitcoin/bitcoin/releases/latest'
        response = urllib.request.urlopen(url1).read()
        data = json.loads(response.decode('utf-8'))
        return data['name']

    def lnd_version():
        url = 'https://api.github.com/repos/lightningnetwork/lnd/releases/latest'
        response = urllib.request.urlopen(url).read()
        data = json.loads(response.decode('utf-8'))
        return data['name']

    def lnd_download_url():
        url = 'https://api.github.com/repos/lightningnetwork/lnd/releases/latest'
        response = urllib.request.urlopen(url).read()
        data = json.loads(response.decode('utf-8'))
        i = 0
        for assets in data['assets']:
            if 'linux-armv7' not in data['assets'][i]['name']:
                i += 1
            else:
                lng_download_url = (data['assets'][i]['browser_download_url'])
                break
        return lng_download_url

    def lnd_file_name():
        url = 'https://api.github.com/repos/lightningnetwork/lnd/releases/latest'
        response = urllib.request.urlopen(url).read()
        data = json.loads(response.decode('utf-8'))
        i = 0
        for assets in data['assets']:
            if 'linux-armv7' not in data['assets'][i]['name']:
                i += 1
            else:
                lnd_file_name = (data['assets'][i]['name'])
                break
        return lnd_file_name


class create_service():

    def bitcoind():
        f = open(soft_folder() + '/bitcoind.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=Bitcoin daemon\n')
        f.write('After=network.target\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + systeminfo.username() + '\n')
        # f.write('Group=' + systeminfo.user_group() + '\n')
        f.write('\n')
        f.write('Type=forking\n')
        f.write(
            'ExecStart=' + bitcoin_folder() + '/bin/bitcoind -conf=' + bitcoin_data_folder() + '/bitcoin.conf -datadir=' + bitcoin_data_folder() + ' -pid=/run/bitcoind/bitcoind.pid\n')
        # f.write('PIDFile=' + bitcoin_data_folder() + '/bitcoind.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()

    def lnd():
        f = open(soft_folder() + '/lnd.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=LND Lightning Daemon\n')
        f.write('Requires=bitcoind.service\n')
        f.write('After=bitcoind.service\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + systeminfo.username() + '\n')
        # f.write('Group=' + systeminfo.user_group() + '\n')
        f.write('\n')
        f.write('Type=simple\n')
        f.write('ExecStart=' + lnd_folder() + '/lnd\n')
        f.write('PIDFile=' + lnd_data_folder() + '/lnd.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()

    def mongo_server():
        f = open(soft_folder() + '/mongod.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=MongoDB Database Server\n')
        f.write('After=multi-user.target\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + systeminfo.username() + '\n')
        f.write('ExecStart=' + mongo_folder() + '/mongod --dbpath /home/pi/soft/mongo/data/db --logpath '
                                                '/home/pi/soft/mongo/data/log/mongodb.log --journal\n')
        f.write('\n')
        f.write('Type=forking\n')
        f.write('PIDFile=' + mongo_folder() + '/mongod.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()

    def portal_server():
        f = open(soft_folder() + '/portal_server.service', 'w+')
        f.write('[Unit]\n')
        f.write('Description=Portal Server Daemon\n')
        f.write('Requires=mongod.service\n')
        f.write('After=mongd.service\n')
        f.write('\n')
        f.write('[Service]\n')
        f.write('User=' + systeminfo.username() + '\n')
        # f.write('Group=' + systeminfo.user_group() + '\n')
        f.write('\n')
        f.write('Type=simple\n')
        f.write('ExecStart=/usr/bin/node ' + portal_server_folder() + '/server.js\n')
        f.write('PIDFile=' + portal_server_folder() + '/server.pid\n')
        f.write('\n')
        f.write('Restart=always\n')
        f.write('\n')
        f.write('[Install]\n')
        f.write('WantedBy=multi-user.target\n')
        f.close()


def sudo_call(fn):
    code = inspect.getsource(fn)
    script = systeminfo.home_folder() + '/sc.py'
    with open(script, "w+") as f:
        f.write(code)
        f.write("%s" % (fn.__name__) + '()')
    f.close()
    os.system('sudo python3 ' + script)


def check_bitcoin_last_version():
    date = datetime(1979, 2, 2).date()
    version = urllib.request.urlopen('https://bitcoincore.org/bin/')
    for line in version:
        match = re.search(r'\d{2}-\w{3}-\d{4}', str(line))
        if match:
            if datetime.strptime(match.group(), '%d-%b-%Y').date() > date:
                date = datetime.strptime(match.group(), '%d-%b-%Y').date()
                last_version = re.findall(r'"([^"]*)"', str(line))
    last_version = ''.join(last_version)
    last_version = last_version.split("-")[2][:-1]
    return last_version


def create_soft_folder():
    # ~/soft/bitcoin/version
    # ~/soft/lnd/version
    folder = systeminfo.home_folder() + '/soft'
    bitcoin_folder = folder + '/bitcoin'
    lnd_folder = folder + '/lnd'
    prereq_folder = folder + '/prereq'
    mongo_db = folder + '/mongo/data/db'
    mongo_log = folder + '/mongo/data/log'
    bitcoin_path = Path(bitcoin_folder)
    lnd_path = Path(lnd_folder)
    prereq_path = Path(prereq_folder)
    mongo_db_path = Path(mongo_db)
    mongo_log_path = Path(mongo_log)
    bitcoin_path.mkdir(parents=True)
    lnd_path.mkdir(parents=True)
    prereq_path.mkdir(parents=True)
    mongo_db_path.mkdir(parents=True)
    mongo_log_path.mkdir(parents=True)


def bitcoin_last_version_url():
    date = datetime(1979, 2, 2).date()
    version = urllib.request.urlopen('https://bitcoincore.org/bin/')
    for line in version:
        match = re.search(r'\d{2}-\w{3}-\d{4}', str(line))
        if match:
            if datetime.strptime(match.group(), '%d-%b-%Y').date() > date:
                date = datetime.strptime(match.group(), '%d-%b-%Y').date()
                btc_version = re.findall(r'"([^"]*)"', str(line))
    btc_version = ''.join(btc_version)
    url = 'https://bitcoincore.org/bin/' + btc_version
    link = urllib.request.urlopen(url)
    for line in link:
        match = re.search('arm', str(line))
        if match:
            url_final_part = re.findall(r'"([^"]*)"', str(line))
            url_final_part = ''.join(url_final_part)
            final_url = 'https://bitcoincore.org/bin/' + btc_version + url_final_part
    return final_url, url_final_part


def home_folder():
    home_fold = os.path.expanduser("~")
    return home_fold


def soft_folder():
    home = os.path.expanduser("~")
    soft_folder = home + "/soft"
    return soft_folder


def pre_folder():
    home = os.path.expanduser("~")
    pre_folder = home + "/soft/prereq"
    return pre_folder


def bitcoin_folder():
    home = os.path.expanduser("~")
    bitcoin_folder = home + "/soft/bitcoin"
    return bitcoin_folder


def lnd_folder():
    home = os.path.expanduser("~")
    lnd_folder = home + "/soft/lnd"
    return lnd_folder


def bitcoin_data_folder():
    home = os.path.expanduser("~")
    btcdir = home + "/.bitcoin"
    return btcdir


def lnd_data_folder():
    home = os.path.expanduser("~")
    lnddir = home + "/.lnd"
    return lnddir


def portal_folder():
    home = os.path.expanduser("~")
    portaldir = home + "/soft/portal"
    return portaldir


def portal_server_folder():
    server_folder = portal_folder() + '/server'
    return server_folder

def mongo_folder():
    m_folder = soft_folder() + '/mongo'
    return m_folder

def mongo_db_folder():
    db_folder = soft_folder() + '/mongo/data/db'
    return db_folder


def mongo_log_folder():
    log_folder = soft_folder() + '/mongo/data/log'
    return log_folder


def is_installed_bitcoin():
    # check if bitcoin is installed
    if os.path.exists(bitcoin_data_folder()):
        return True
    else:
        return False


def is_installed_lnd():
    # check if LND is installed
    if os.path.exists(lnd_data_folder()):
        return True
    else:
        return False


def is_running(process):
    # check if proccess exists
    pid = subprocess.Popen(['pgrep', process], stdout=subprocess.PIPE)
    out, err = pid.communicate()
    if out:
        return True
    else:
        return False


def Reinstall_all():
    final_url, url_final_part = bitcoin_last_version_url()
    if is_running('bitcoind'):
        print("\033[1;32;40m Stopping bitcoind  \n")
        subprocess.call(["bitcoin-cli", "stop"])
    if is_installed_bitcoin():
        print("\033[1;32;40m Deleting Bitcoin data folder  \n")
        # subprocess.call("rm","-R", bitcoin_data_folder())
    if os.path.exists(soft_folder()):
        print("\033[1;32;40m Deleting soft folder  \n")
        subprocess.call(['rm', '-R', soft_folder()])
        create_soft_folder()
    else:
        create_soft_folder()
    Install_Bitcoin_Core()


def Install_Bitcoin_Core():
    final_url, url_final_part = bitcoin_last_version_url()
    print('\033[1;32;40m Downloading Bitcoin Core ' + check_bitcoin_last_version() + ' \n')
    color_text.reset_style()
    logging.info('Downloading Bitcoin Core')
    subprocess.call(['wget', final_url, '-P', bitcoin_folder()])
    logging.info('Download finished')
    color_text.info('Installing Bitcoin Core' + check_bitcoin_last_version())
    logging.info('Installing Bitcoin Core')
    subprocess.call(
        ['tar', '-xf', bitcoin_folder() + '/' + url_final_part, '--strip-components=1', '-C', bitcoin_folder()])
    subprocess.call(['rm', bitcoin_folder() + '/' + url_final_part])
    logging.info('Add Bitcoin Core to path')
    add_PATH_Bitcoin()


def Bitcoin_conf_file(RPC_user, RPC_pass, prune_value):
    path = Path(bitcoin_data_folder())
    path.mkdir(parents=True)
    conf_file = open(bitcoin_data_folder() + '/bitcoin.conf', 'w')
    conf_file.write('# Generated by Dlightning installation script\n')
    conf_file.write('\n\n')
    conf_file.write('# Run Bitcoin Core as daemon\n')
    conf_file.write('daemon=1\n')
    conf_file.write('\n\n')
    conf_file.write('# RPC configuration\n')
    conf_file.write('rpcuser=' + RPC_user + '\n')
    conf_file.write('rpcpassword=' + RPC_pass + '\n')
    conf_file.write('\n\n')
    conf_file.write('# Pruning value\n')
    conf_file.write('prune=' + str(prune_value) + '\n')
    conf_file.write('\n\n')
    conf_file.write('# ZMQ configuration\n')
    conf_file.write('zmqpubrawblock=tcp://127.0.0.1:18501\n')
    conf_file.write('zmqpubrawtx=tcp://127.0.0.1:18502\n')
    conf_file.close()

def Bitcoin_data():
    subprocess.call(['wget', 'http://utxosets.blob.core.windows.net/public/utxo-snapshot-bitcoin-mainnet-551636.tar', '-O', 'data.tar'])
    color_text.info('Copying Bitcoin data file')
    subprocess.call(['tar', '-xf', 'data.tar', '--keep-old-files', '-C', bitcoin_data_folder()])
    color_text.info('Data folder copied')
    subprocess.call(['rm', 'data.tar'])

def Install_LND():
    print('\033[1;32;40m Downloading ' + github.lnd_version() + ' \n')
    color_text.reset_style()
    subprocess.call(['wget', github.lnd_download_url(), '-P', lnd_folder()])
    color_text.info('Installing ' + github.lnd_version() + ' \n')
    subprocess.call(
        ['tar', '-xf', lnd_folder() + '/' + github.lnd_file_name(), '--strip-components=1', '-C', lnd_folder()])
    subprocess.call(['rm', lnd_folder() + '/' + github.lnd_file_name()])
    add_PATH_LND()


def LND_conf_file(RPC_user, RPC_pass, LND_name):
    path = Path(lnd_data_folder())
    path.mkdir(parents=True)
    conf_file = open(lnd_data_folder() + '/lnd.conf', 'w')
    conf_file.write('# Generated by Dlightning installation script\n')
    conf_file.write('\n\n')
    conf_file.write('# Configuration\n')
    conf_file.write('bitcoin.active=1\n')
    conf_file.write('alias=' + LND_name + '\n')
    conf_file.write('color=#1d8c09\n')
    conf_file.write('bitcoin.active=1\n')
    conf_file.write('bitcoin.mainnet=1\n')
    conf_file.write('bitcoin.node=bitcoind\n')
    conf_file.write('\n\n')
    conf_file.write('# RPC configuration\n')
    conf_file.write('bitcoind.rpcuser=' + RPC_user + '\n')
    conf_file.write('bitcoind.rpcpass=' + RPC_pass + '\n')
    conf_file.write('# ZMQ configuration\n')
    conf_file.write('bitcoind.zmqpubrawblock=tcp://127.0.0.1:18501\n')
    conf_file.write('bitcoind.zmqpubrawtx=tcp://127.0.0.1:18502\n')
    conf_file.close()


def Install_Portal():
    print('\033[1;32;40m Downloading user portal. \n')
    subprocess.call(['git', 'clone', 'https://gitlab.com/aitoribanez/dlightning-front', portal_folder()])
    print('\033[1;32;40m Installing user portal. \n')
    os.system('npm install ' + portal_folder() + '/server/')


def add_PATH_Bitcoin():
    home = os.path.expanduser("~")
    with open(home + '/.profile', 'a') as file:
        file.write('export PATH="$PATH:' + bitcoin_folder() + '/bin"\n')
        file.close()


def add_PATH_LND():
    home = os.path.expanduser("~")
    with open(home + '/.profile', 'a') as file:
        file.write('export PATH="$PATH:' + lnd_folder() + '"\n')
        file.close()


def download_blockchain():
    print('\033[1;32;40m Downloading Bitcoin data folder \n')
    color_text.reset_style()
    subprocess.call(['wget https://file.io/pepelu', '-P', soft_folder()])
    color_text.info('Uncompressing data folder \n')
    subprocess.call(
        ['tar', '-xf ', '--strip-components=1', '-C', bitcoin_data_folder()])
    subprocess.call(['rm pepelu'])


def reinstallmenu():
    confirm = input("Are you sure? All data will be lost! (y/n)")
    if confirm.lower() == "y":
        Reinstall_all()
    if confirm.lower() == "n":
        mainmenu()
    else:
        print("Please insert a valid option.")


def mainmenu():
    os.system('clear')
    color_text.info('Welcome to Dlightning node configuration \n')
    print("1. Reinstall all \n 2. Update all \n 3. Configure \n 4. Quit")
    choice = int(input("Enter option --> "))
    if choice == 1:
        reinstallmenu()
    if choice == 2:
        Update_all()
    if choice == 3:
        Configure_all()
    if choice == 4:
        exit()
    else:
        print("Please insert a valid option")
        mainmenu()


def microsd_size():
    total, used, free = shutil.disk_usage("/")
    microsd_size = (total // (2 ** 30))
    return microsd_size


def pruning_value():
    pruning_value = math.ceil(microsd_size() / 1.5)
    return pruning_value


def prerequisites():
    # Add nodejs repository (install 10.x nodejs)
    # color_text.info('Adding NodeJS repository')
    import os
    soft_folder = '/home/' + (os.environ['SUDO_USER']) + '/soft'
    pre_folder = '/home/' + (os.environ['SUDO_USER']) + '/soft/prereq'
    mongo_folder = '/home/' + (os.environ['SUDO_USER']) + '/soft/mongo'
    web_folder = '/home/' + (os.environ['SUDO_USER']) + '/dlightning/PortalFE'
    screen_driver_folder = '/home/' + (os.environ['SUDO_USER']) + '/dlightning/ScreenDriver'
    f = open('/etc/apt/sources.list.d/nodesource.list', 'w+')
    f.write('deb https://deb.nodesource.com/node_10.x stretch main\n')
    f.write('deb-src https://deb.nodesource.com/node_10.x stretch main\n')
    f.close()
    # color_text.info('Updating repository info')
    # color_text.reset_style()
    os.system('apt-get update')
    # color_text.info('Updating system')
    # color_text.reset_style()
    os.system('apt-get upgrade -y')
    # color_text.info('Installing prerequisites')
    # color_text.reset_style()
    os.system('apt-get install -y --allow-unauthenticated nodejs git apache2 python3-pip samba')
    os.system('npm install coffeescript -g')
    os.system(
        'wget http://security.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.0.0_1.0.1t-1'
        '+deb8u9_armhf.deb -P ' + pre_folder)
    os.system('dpkg -i ' + pre_folder + '/libssl1.0.0_1.0.1t-1+deb8u9_armhf.deb')
    os.system('wget https://andyfelong.com/downloads/core_mongodb.tar.gz -P ' + mongo_folder)
    os.system('tar -xzf ' + mongo_folder + '/core_mongodb.tar.gz -C ' + mongo_folder)
    os.system('rm ' + mongo_folder + '/core_mongodb.tar.gz')
    os.system('export LC_ALL=C')
    # Install screen driver (always or user activated?)
    os.chdir(screen_driver_folder)
    os.system('cp tft35a-overlay.dtb /boot/overlays/')
    os.system('cp tft35a-overlay.dtb /boot/overlays/tft35a.dtbo')
    os.system('cp cmdline.txt /boot/')
    os.system('cp inittab /etc/')
    os.system('cp config-35.txt /boot/config.txt')
    # Create services
    os.chdir(soft_folder)
    os.system('cp bitcoind.service /etc/systemd/system/')
    os.system('cp lnd.service /etc/systemd/system/')
    os.system('cp portal_server.service /etc/systemd/system/')
    os.system('cp mongod.service /etc/systemd/system/')
    os.system('systemctl enable bitcoind.service')
    os.system('systemctl enable lnd.service')
    os.system('systemctl enable portal_server.service')
    os.system('systemctl enable mongod.service')
    # Copy portal web
    os.system('tar -xf ' + web_folder + '/portalFE.tar.gz --strip-components=1 -C  /var/www/html/')
    f = open('/etc/issue', 'w')
    f.write('\33[3J\33[H\33[2J\33[1;32mWelcome to Dlightning Node v0.101\33[0m\n')
    f.write('\n\n')
    f.write(r'Today is \d \t @ \n')
    f.write('\n\n\n')
    f.write('\33[1;32mPlease connect to \4{eth0} from a web browser.\33[0m\n')
    f.close()


# noinspection PyCallByClass
class user_input():

    def check_answer(answer):
        a1 = answer[0].lower()
        if answer == '' or not a1 in ['y', 'n']:
            color_text.warning('Please answer y or n')
            return False
        else:
            return True

    def check_password(RPC_pass, confirm_pass):
        if RPC_pass == confirm_pass:
            return True
        else:
            return False

    def RPC_user():
        color_text.change_to_green()
        RPC_user = input(' Insert RPC username ---> ')
        color_text.reset_style()
        return RPC_user

    def RPC_pass():
        color_text.change_to_green()
        RPC_pass = getpass.getpass(prompt=' Insert RPC password ---> ')
        confirm_pass = getpass.getpass(prompt=' Repeat RPC password ---> ')
        color_text.reset_style()
        if user_input.check_password(RPC_pass, confirm_pass):
            return RPC_pass
        else:
            color_text.warning('Passwords don\'t match, try again.')
            user_input.RPC_pass()
        color_text.reset_style()

    def LND_name():
        color_text.change_to_green()
        LND_name = input(' Insert your LND node name ---> ')
        color_text.reset_style()
        return LND_name

    def tor_setup():  # mirar bien esta función más adelante
        color_text.change_to_green()
        tor_setup = input(' Do you want to use TOR (if not sure answer NO  ---> ')
        if user_input.check_answer(tor_setup):
            if tor_setup.lower() in ['y']:
                # print('devuelvo y')
                return 'y'
            else:
                # print('devuelvo n')
                return 'n'
        else:
            user_input.tor_setup()

    def download_chain():
        color_text.change_to_green()
        download_chain = input(' Do you want to use TOR (if not sure answer NO  ---> ')
        if user_input.check_answer(download_chain):
            if download_chain.lower() in ['y']:
                # print('devuelvo y')
                return True
            else:
                # print('devuelvo n')
                return False
        else:
            user_input.tor_setup()


def first_run():
    os.system('clear')
    color_text.info('Welcome to Dlightning node installation')
    logging.info(str(microsd_size()) + ' GB microsd')
    logging.info('Prune set to ' + str(pruning_value()) + ' GB')
    color_text.info('Detected ' + str(microsd_size()) + ' GB microsd, node pruning will set to ' + str(pruning_value()) + ' GB\n')
    prune_value = pruning_value() * 1000
    color_text.info('Please take a moment to give some data:')
    logging.info('User data')
    RPC_user = user_input.RPC_user()
    RPC_pass = user_input.RPC_pass()
    LND_name = user_input.LND_name()
    # setup_TOR = user_input.tor_setup()  # we will have y or n in setup_TOR variable
    if user_input.download_chain():
        download_blockchain()
    # posibility of download bitcoin data folder??
    color_text.info('Creating folder structure')
    logging.info('Generating folder tree')
    create_soft_folder()
    logging.info('Generating Bitcoin Core daemon service')
    create_service.bitcoind()
    logging.info('Generating LND daemon service')
    create_service.lnd()
    logging.info('Generating Mongo service')
    create_service.mongo_server()
    logging.info('Generating portal server daemon service')
    create_service.portal_server()
    logging.info('Installing prerequisites')
    sudo_call(prerequisites)
    logging.info('Installing Bitcoin Core')
    Install_Bitcoin_Core()
    logging.info('Generating Bitcoin Core configuration file')
    color_text.info('Generating Bitcoin configuration file')
    Bitcoin_conf_file(RPC_user, RPC_pass, prune_value)
    color_text.info('Downloading Bitcoin data folder')
    Bitcoin_data()
    logging.info('Installing LND')
    Install_LND()
    logging.info('Generating LND configuration file')
    color_text.info('Generating LND configuration file')
    LND_conf_file(RPC_user, RPC_pass, LND_name)
    logging.info('Installing Portal')
    Install_Portal()
    logging.info('Finish')


def main():

    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='dlightning.log', level=logging.DEBUG)
    logging.info('Dlightning Node installation started')
    if os.path.exists(soft_folder()):
        logging.info('Soft folder exists --> to mainmenu')
        mainmenu()
    else:
        logging.info('First run detected')
        first_run()


if __name__ == "__main__":
    main()
